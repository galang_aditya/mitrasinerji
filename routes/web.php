<?php

use App\Http\Controllers\Admin\AjaxController;
use App\Http\Controllers\admin\AutonumberController;
use App\Http\Controllers\Admin\BarangController;
use App\Http\Controllers\Admin\CostumerController;
use App\Http\Controllers\Admin\SalesController;
use App\Http\Controllers\Admin\StockController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('home',function(){
    return view('backoffice.dashboard.index');
});

// Route::get('costumer',function(){
//     return view('backoffice.costumer.index');
// });

Route::get('barang',function(){
    return view('backoffice.barang.index');
});



Route::resource('barang',BarangController::class);
Route::resource('costumer',CostumerController::class);
Route::resource('stock', StockController::class);
Route::resource('sales', SalesController::class);
Route::get('stck_autonum',[AutonumberController::class,'autoNumTransc'])->name('autonum-transc');
Route::get('stock_search',[AjaxController::class,'index'])->name('stock_search');


