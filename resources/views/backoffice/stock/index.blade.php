@extends('backoffice.layout.master')
@section('main')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active">Stock</li>
@endsection
@section('content')
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Stock</h5>
                    <!-- Table with stripped rows -->
                    <div class="text-left">
                        <button type="buttom" id="addStock" class="btn btn-primary">Tambah Stock</button>
                    </div>
                    <br>
                    <table class="table stock">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Barang</th>
                                <th scope="col">stock</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>

                        @php
                            $no =1 ;
                            $idnum =1 ;
                            $btnChil=1 ;
                        @endphp
                        @foreach ($stocks as $stock )
                        <tbody>
                            <tr>
                                <th scope="row">{{ $no++ }}</th>
                                <td>{{ $stock->barangs->nama ?? 'barang tidak ada' }}</td>
                                <td>{{ $stock->stock }}</td>
                                <td><a href="#" id="edit{{ $idnum++ }}" class="btn btn-primary"
                                        data-id="{{ route('stock.edit',$stock) }}" onclick="addStock({{ $btnChil++ }})"> Edit
                                    </a> | <a href="#" id="deleteStock" class="btn btn-danger"> Hapus </a></td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    <!-- End Table with stripped rows -->

                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="basicModal" tabindex="-1">
    <div class="modal-dialog">
        <form action="{{route('stock.store')}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Stock</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <!-- Vertical Form -->
                            <h5 class="card-title">Tambah Stock</h5>
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Nama Barang</label>
                                <select class="form-select" name="id_barang" aria-label="Default select example">
                                </select>
                            </div>
                            <div class="col-12">
                                <label for="inputEmail4" class="form-label">Stock</label>
                                <input type="number" class="form-control" name="stock">
                            </div>
                            <!-- Vertical Form -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div>
    <div class="modal fade" id="modalDelete" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <form class="row g-3" action="{{ route('stock.destroy',$stock) }}" method="POST">
                @method('DELETE') @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Hapus Stock</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Anda Yakin Ingin Mengahpus data?
                    </div>
                    <div style="text-align: center">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Hapus</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>


<div>
    <div class="modal fade" id="editStock" tabindex="-1">
        <div class="modal-dialog">
            <form action="{{route('stock.update',$stock)}}" method="POST">
                @csrf @method('PUT')
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Stock</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-body">
                                <!-- Vertical Form -->
                                <h5 class="card-title">Tambah Stock</h5>
                                <div class="col-12">
                                    <label for="inputNanme4" class="form-label">Nama Barang</label>
                                    <select class="form-select barangs" name="id_barang" aria-label="Default select example">
                                    </select>
                                </div>
                                <div class="col-12">
                                    <label for="inputEmail4" class="form-label">Stock</label>
                                    <input type="number" class="form-control" id="stock-count" name="stock">
                                </div>
                                <!-- Vertical Form -->
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    $(document).ready(function(){

        var src = "{{ route('stock_search') }}";
        $('.stock').DataTable();

        $('#addStock').click(function(){
            $('#basicModal').modal('show');
            $('.form-select').select2({
                theme: 'bootstrap-5',
                dropdownParent: $('#basicModal'),
                ajax: {
                    url : src,
                    dataType : 'json',
                    delay : 250,
                    processResults : function(resposne){
                        return {
                            results : $.map(resposne, function (item){
                                return {
                                    text : item.nama,
                                    id : item.id
                                }
                            })
                        }
                    }
                },
                cache: true
            })
        })

        $('#deleteStock').click(function(){
            $('#modalDelete').modal('show')
        })

    })

    function addStock($id){
        var btn = "edit"+$id
        var src = "{{ route('stock_search') }}";
        var path = $(''+'#'+btn+'').attr('data-id');
        $.ajax({
            type : "GET",
            url : path,
            success : function(response){
                console.log(response)
                $("#editStock").modal('show');
                $('.form-select').append('<option value="' + response.data.id + '">' + response.barang.name + '</option>')
                $('#stock-count').val(response.data.stock)

            }
        })
        $('.barangs').select2({
            theme: 'bootstrap-5',
            dropdownParent: $('#editStock'),
            ajax: {
                url : src,
                dataType : 'json',
                delay : 250,
                processResults : function(resposne){
                    return {
                        results : $.map(resposne, function (item){
                            return {
                                text : item.nama,
                                id : item.id
                            }
                        })
                    }
                }
            },
            cache: true
        })

    }

</script>
@endpush('js')

