@extends('backoffice.layout.master')
@section('pagetitle')
<h1>Costumer</h1>
@endsection
@section('content')
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Costumer</h5>
                    <!-- Table with stripped rows -->
                    <div class="text-left">
                        <button type="buttom" id="addCust" class="btn btn-primary">Tambah Costumer</button>
                    </div>
                    <br>
                    @php
                    $no=1;
                    $id=1;
                    $btn=1;
                    @endphp
                    <table class="table Costumer">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Tlp</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        @foreach ($costumers as $costumer )
                        <tbody>
                            <tr>
                                <th scope="row">{{$no++}}</th>
                                <td>{{ $costumer->kode }}</td>
                                <td>{{ $costumer->name }}</td>
                                <td>{{ $costumer->tlp }}</td>
                                <td><a href="#" class="btn btn-danger" onclick="deleteModal()">Hapus</a> | <a href="#" id="edit{{ $id++ }}" class="btn btn-warning"
                                        onclick="editCust({{ $btn++ }})"
                                        data-id="{{ route('costumer.edit',$costumer) }}">Edit</a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    <!-- End Table with stripped rows -->

                </div>
            </div>

        </div>
    </div>
</section>
<!-- Modal Add Costumer -->
<div class="modal fade" id="fromCust" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form class="row g-3" action="{{ route('costumer.store') }}" method="POST">
            @method('POST') @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">From Tambah Costumer</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <!-- Vertical Form -->
                            <h5 class="card-title">Costumer</h5>
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Kode</label>
                                <input type="text" class="form-control" name="kode">
                            </div>
                            <div class="col-12">
                                <label for="inputEmail4" class="form-label">Nama</label>
                                <input type="text" class="form-control" name="name">
                            </div>
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Telp</label>
                                <input type="text" class="form-control" name="tlp">
                            </div>
                            <!-- Vertical Form -->
                        </div>
                    </div>
                </div>
                <div style="text-align: center">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
        </form>
    </div>
</div>

</div>
<div>
    <!-- Modal Edit Costumer -->
    <div class="modal fade" id="verticalycentered" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <form class="row g-3" action="{{ route('costumer.update',$costumer) }}" method="POST">
                @method('PUT') @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">From Edit Costumer</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-body">
                                <!-- Vertical Form -->
                                <h5 class="card-title">Costumer Data</h5>
                                <div class="col-12">
                                    <label for="inputNanme4" class="form-label">Kode</label>
                                    <input type="text" class="form-control" id="code" name="kode">
                                </div>
                                <div class="col-12">
                                    <label for="inputEmail4" class="form-label">Nama</label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                                <div class="col-12">
                                    <label for="inputNanme4" class="form-label">Telp</label>
                                    <input type="text" class="form-control" id="tlp" name="tlp">
                                </div>
                                <!-- Vertical Form -->
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Delete Costumer -->
<div>
    <div class="modal fade" id="modalDelete" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <form class="row g-3" action="{{ route('costumer.destroy',$costumer) }}" method="POST">
                @method('DELETE') @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">From Edit Costumer</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Anda Yakin Ingin Mengahpus data?
                    </div>
                    <div style="text-align: center">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Hapus</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $('.Costumer').DataTable();
        $('#addCust').click(function(){
            $('#fromCust').modal('show');
        })
    });
    function editCust($id){
         var btn = "edit"+$id
         var url = $(''+ '#' + btn +'') .attr('data-id');
         console.log(url);
            $.ajax({
                type : "GET",
                url : url,
                success : function (data){
                    console.log(data.data.name);
                    $('#verticalycentered').modal('show');
                    $('#name').val(data.data.name);
                    $('#code').val(data.data.kode);
                    $('#tlp').val(data.data.tlp);

                }
            })
    }
    function deleteModal(){
        console.log('test');
        $('#modalDelete').modal('show');
    }
</script>
@endsection
