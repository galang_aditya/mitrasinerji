<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="{{ 'home' }}">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{ 'barang' }}">
          <i class="bi bi-grid"></i>
          <span>Barang</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('stock.index') }}">
          <i class="bi bi-grid"></i>
          <span>Stock</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('costumer.index') }}">
          <i class="bi bi-grid"></i>
          <span>Costumer</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{ 'sales' }}">
          <i class="bi bi-grid"></i>
          <span>Transakis</span>
        </a>
      </li>

    </ul>

  </aside>
