<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Dashboard - NiceAdmin Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" type="text/css">
  <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" type="text/javascript"></script>

  <!-- Favicons -->
  <link href="{{ asset('assets-backoffice') }}/img/favicon.png" rel="icon">
  <link href="{{ asset('assets-backoffice') }}/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!--- Datables -->
  <link href="//cdn.datatables.net/1.13.5/css/jquery.dataTables.min.css" rel="stylesheet">


  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets-backoffice') }}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ asset('assets-backoffice') }}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{ asset('assets-backoffice') }}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{ asset('assets-backoffice') }}/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="{{ asset('assets-backoffice') }}/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="{{ asset('assets-backoffice') }}/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{ asset('assets-backoffice') }}/vendor/simple-datatables/style.css" rel="stylesheet">

  <!-- Jquery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <!-- select2 -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets-backoffice') }}/css/style.css" rel="stylesheet">


  <!-- boostrapSelet2 -->
  <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

  <!-- Costume -->
  @yield('css')

  <!-- =======================================================
  * Template Name: NiceAdmin
  * Updated: May 30 2023 with Bootstrap v5.3.0
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
    @include('backoffice.layout.header')
  <!-- End Header -->

  <!-- ======= Sidebar ======= -->
    @include('backoffice.layout.navbar')
  <!-- End Sidebar-->

  <main id="main" class="main">

    <div class="pagetitle">
      {{--  <h1>Dashboard</h1>  --}}
      @yield('pagetitle')
      <nav>
        <ol class="breadcrumb">
          {{--  <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item active">Dashboard</li>  --}}
          @yield('breadcrumb')
        </ol>
      </nav>
    </div>
    <!-- End Page Title -->

    <section class="section dashboard">
        @yield('content')
    </section>

  </main>
  <!-- End #main -->

  <!-- ======= Footer ======= -->
  @include('backoffice.layout.footer')
  <!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <!-- Datatables -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <script src="{{ asset('assets-backoffice') }}/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="{{ asset('assets-backoffice') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('assets-backoffice') }}/vendor/chart.js/chart.umd.js"></script>
  <script src="{{ asset('assets-backoffice') }}/vendor/echarts/echarts.min.js"></script>
  <script src="{{ asset('assets-backoffice') }}/vendor/quill/quill.min.js"></script>
  <script src="{{ asset('assets-backoffice') }}/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="{{ asset('assets-backoffice') }}/vendor/tinymce/tinymce.min.js"></script>
  <script src="{{ asset('assets-backoffice') }}/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets-backoffice') }}/js/main.js"></script>

  <!-- costume -->
  @yield('js')

  @stack('js')

</body>

</html>
