@extends('backoffice.layout.master')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active">Sales</li>
@endsection
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Sales</h5>
        <div class="text-left">
            <button type="buttom" id="addTrasnc" class="btn btn-primary">Tambah Transaksi</button>
        </div>
        <br>
        <!-- Default Table -->
        <table class="table sales">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">kode</th>
                    <th scope="col">tgl</th>
                    <th scope="col">costumer</th>
                    <th scope="col">subtotal</th>
                    <th scope="col">diskon</th>
                    <th scope="col">ongkir</th>
                    <th scope="col">total bayar</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Brandon Jacob</td>
                    <td>Designer</td>
                    <td>28</td>
                    <td>2016-05-25</td>
                    <td>Designer</td>
                    <td>28</td>
                    <td>2016-05-25</td>
                </tr>
            </tbody>
        </table>
        <!-- End Default Table Example -->
    </div>
</div>

<div>
    <div class="modal fade" id="modalAdd" tabindex="-1">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">From Tambah Transaksi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" style="border: 2ch">

                    <div class="row mb-3">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Transaksi</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="no-transsc">
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Tanggal</label>
                        <div class="col-sm-10">
                          <input type="date" class="form-control" id="date">
                        </div>
                    </div>
                    <br>

                    <div class="row mb-3">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">kode</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputEmail">
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">nama</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="inputPassword">
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">tlp</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="inputPassword">
                        </div>
                      </div>

                    <br>
                    <div class="text-left">
                        <button type="buttom" id="additems" class="btn btn-primary">Tambah</button>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table">
                                <thead>
                                    <tr style="text-align: center">
                                        <th scope="col">kode Barang</th>
                                        <th scope="col">Nama Barang</th>
                                        <th scope="col">qty</th>
                                        <th scope="col">harga Banderol</th>
                                        <th scope="col">Diskon</th>
                                        <th scope="col">(Rp)</th>
                                        <th scope="col">Hagra Diskon</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="text-align: center">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('js')
    <script>
        $(document).ready(function(){

        var rowIdx = 0;
        var rownum = 0;
        var src = "{{ route('stock_search') }}";

        $('.sales').DataTable();

        $('#addTrasnc').click(function(){
            var path =  "{{ route('autonum-transc') }}"
            $('#modalAdd').modal('show')
            $.ajax({
                type : "GET",
                url : path,
                success : function(response){
                    $('#no-transsc').val(response.data)
                }

            })
        })

        $('#additems').click(function(){
            $('#tbody').append(`
            <tr id="row${rowIdx++}">
                <th scope="row">
                    <div class="col-sm-10">
                        <select class="form-select" id="items-src">
                        </select>
                    </div>
                </th>
                <td>
                    <div class="col-sm-12">
                        <input type="text" class="form-control">
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        <input type="text" class="form-control">
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        <input type="text" class="form-control">
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        <input type="text" class="form-control">
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        <input type="text" class="form-control">
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        <input type="text" class="form-control">
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        <input type="text" class="form-control">
                    </div>
                </td>
                <td>
                    <a href="#" class ="btn btn-danger remove" onclick="deleteRow(this)" >Delete</a>
                </td>
            </tr>
            `);
            $('#items-src').select2({
                theme: 'bootstrap-5',
                dropdownParent: $('#modalAdd'),
                ajax: {
                    url : src,
                    dataType : 'json',
                    delay : 250,
                    processResults : function(resposne){
                        return {
                            results : $.map(resposne, function (item){
                                return {
                                    text : item.nama,
                                    id : item.id
                                }
                            })
                        }
                    }
                },
                cache: true
            })
        })
    })
    function deleteRow(ele){
        $(ele).parent().parent().remove();
    }
    </script>
    @endpush
