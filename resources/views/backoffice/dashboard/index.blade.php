@extends('backoffice.layout.master')
@section('pagetitle')
<h1>Dashboard</h1>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="index.html">Home</a></li>
<li class="breadcrumb-item active">Dashboard</li>
@endsection
@section('content')
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Transaksi</h5>
                    <!-- Table with stripped rows -->
                    <div class="text-left">
                        <button type="buttom" id="addTransc" class="btn btn-primary">Tambah Transaksi</button>
                    </div>
                    <br>
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Position</th>
                                <th scope="col">Age</th>
                                <th scope="col">Start Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Brandon Jacob</td>
                                <td>Designer</td>
                                <td>28</td>
                                <td>2016-05-25</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Bridie Kessler</td>
                                <td>Developer</td>
                                <td>35</td>
                                <td>2014-12-05</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Ashleigh Langosh</td>
                                <td>Finance</td>
                                <td>45</td>
                                <td>2011-08-12</td>
                            </tr>
                            <tr>
                                <th scope="row">4</th>
                                <td>Angus Grady</td>
                                <td>HR</td>
                                <td>34</td>
                                <td>2012-06-11</td>
                            </tr>
                            <tr>
                                <th scope="row">5</th>
                                <td>Raheem Lehner</td>
                                <td>Dynamic Division Officer</td>
                                <td>47</td>
                                <td>2011-04-19</td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- End Table with stripped rows -->

                </div>
            </div>

        </div>
    </div>
</section>

<div class="modal fade" id="largeModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form class="row g-3">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">From Tambah Transaksi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">

                        <!-- Vertical Form -->
                        <h5 class="card-title">Transaksi</h5>
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">No</label>
                                <input type="text" class="form-control" id="no">
                            </div>
                            <div class="col-12">
                                <label for="inputEmail4" class="form-label">Tanggal</label>
                                <input type="date" class="form-control" id="inputEmail4">
                            </div>
                            <br>
                            <h5 class="card-title">Costumer</h5>
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Kode</label>
                                <input type="text" class="form-control" id="no">
                            </div>
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Nama</label>
                                <input type="text" class="form-control" id="no">
                            </div>
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Telp</label>
                                <input type="number" class="form-control" id="no">
                            </div>
                        <!-- Vertical Form -->

                    </div>
                </div>
            </div>
            <div style="text-align: center">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- End Large Modal-->

@endsection

@section('js')
<script>
    $(document).ready(function(){
        $('.btn').click(function(){
            $('#largeModal').modal('show')
        });

        $(function(){
            var table = $('')
        })
    })
</script>
@endsection
