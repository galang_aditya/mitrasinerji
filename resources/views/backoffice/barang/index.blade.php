@extends('backoffice.layout.master')
@section('pagetitle')
<h1>Barang</h1>
@endsection
@section('content')
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Default Table</h5>
                    <div class="text-left">
                        <button type="buttom" id="addItems" class="btn btn-primary">Tambah Transaksi</button>
                    </div>
                    <!-- Default Table -->
                    <table class="table" id="mytable" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">NO</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Harga</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        @php
                        $no=1;
                        $num =1;
                        $id=1;
                        @endphp
                        @foreach ($barangs as $barang )
                        <tbody>
                            <tr>
                                <th scope="row">{{ $no++ }}</th>
                                <td>{{ $barang->kode }}</td>
                                <td>{{ $barang->nama }}</td>
                                <td>Rp.{{ number_format($barang->harga,3) }}</td>
                                <td>

                                    <form action="{{ route('barang.destroy',$barang) }}" method="POST">
                                        <input type="hidden" class="form-control" name="url" id="edit{{ $num++}}"value="{{ route('barang.edit',$barang) }}">
                                        <a href="javascript:void(0)" onclick="editData({{ $id++ }})" class="btn btn-primary edit">Edit</a>
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    <!-- End Default Table Example -->
                </div>
            </div>

        </div>
    </div>
</section>


{{-- Modal Edit --}}

<div class="modal fade" id="verticalycentered" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <form action="{{ route('barang.update',$barang) }}" method="POST">
            @csrf @method("PUT")
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Barang</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <!-- Vertical Form -->
                        <h5 class="card-title">Data Barang</h5>
                        <div class="col-12">
                            <label for="inputNanme4" class="form-label">Kode</label>
                            <input type="text" class="form-control" id="code" name="kode">
                        </div>
                        <div class="col-12">
                            <label for="inputNanme4" class="form-label">Nama</label>
                            <input type="text" class="form-control" id="name" name="nama">
                        </div>
                        <div class="col-12">
                            <label for="inputNanme4" class="form-label">Harga</label>
                            <input type="number" class="form-control" id="price" name="harga">
                        </div>
                        <!-- Vertical Form -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="Submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
        </div>
    </div>
</div>
{{-- modal add --}}
<div class="modal fade" id="Modalitems" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form class="row g-3" action="{{ route('barang.store') }}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">From Tambah Barang</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <!-- Vertical Form -->
                            <h5 class="card-title">Data Barang</h5>
                            <h5 class="card-title">Barang</h5>
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Kode</label>
                                <input type="text" class="form-control" name="kode">
                            </div>
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Nama</label>
                                <input type="text" class="form-control" name="nama">
                            </div>
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Harga</label>
                                <input type="number" class="form-control" name="harga">
                            </div>
                            <!-- Vertical Form -->
                        </div>
                    </div>
                </div>
                <div style="text-align: center">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="Submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
        </form>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable();
        $('#addItems').click(function(){
            $('#Modalitems').modal('show');
        })
    });

    function editData($id){
        var  btn = "edit"+$id
        var test = $(''+ '#' + btn +'').val();
           $.ajax({
               type: "GET",
               url : test,
               success : function(data){
                   $('#verticalycentered').modal('show')
                   $('#code').val(data.data.kode)
                   $('#name').val(data.data.nama)
                   $('#price').val(data.data.harga)
               }
           })
    }
</script>

@endsection
