<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Autonumber extends Model
{
    use HasFactory;

    protected $table = 'autonumbers';

    protected $fillable = [
        'no_trans'
    ];
}
