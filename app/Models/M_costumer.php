<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_costumer extends Model
{
    use HasFactory;
    protected $table = 'm_costumers';
    protected $fillable =[
        'kode',
        'name',
        'tlp'
    ];
}
