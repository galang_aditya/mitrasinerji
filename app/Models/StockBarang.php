<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\M_barang;

class StockBarang extends Model
{
    use HasFactory;

    protected $table = 'stock_barangs';
    protected $fillable = [
        'stock',
        'id_barang'
    ];

    public function barangs(){
        return $this->belongsTo(M_barang::class,'id_barang','id');
    }
}
