<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StockBarang;
use App\Models\M_barang;

class AjaxController extends Controller
{
    //

    public function index(Request $request){
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data = M_barang::select('id','nama')
                    ->where('nama','LIKE',"%$search%")
                    ->get();
        }
        return response()->json($data);

    }
}
