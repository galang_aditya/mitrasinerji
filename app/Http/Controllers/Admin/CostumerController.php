<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\M_costumer;
use App\http\Requests\CostumerRequestStore;
use App\http\Requests\CostumerRequestUpdate;

class CostumerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $costumers = M_costumer::all();
        return view('backoffice.costumer.index',compact('costumers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CostumerRequestStore $request)
    {
        //
        $data = $request->all();
        // dd($data);
        $costumer = new M_costumer($data);
        $costumer->save();

        return redirect()->route('costumer.index');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(M_costumer $costumer)
    {
        //
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $costumer
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CostumerRequestUpdate $request,M_costumer  $costumer)
    {
        //
        $costumer->update($request->all());

        return redirect()->route('costumer.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(M_costumer $costumer)
    {
        //
        $costumer->delete();
        return redirect()->route('costumer.index');
    }
}
