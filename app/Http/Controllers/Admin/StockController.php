<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StockBarang;
use App\Http\Requests\stockStorerequest;
use App\Http\Requests\stockUpdatequest;
use App\Models\M_barang;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $stocks = StockBarang::all();
        return view('backoffice.stock.index',compact('stocks'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(stockStorerequest $request,StockBarang $stock)
    {
        //
        $data = $request->all();
        $stock = new StockBarang($data);
        $stock->save();

        return redirect()->route('stock.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(StockBarang $stock)
    {
        //
        $id = $stock->id_barang;

        $items = M_barang::where('id',"=",$id)->get();

        foreach ($items as $item){
            $dataitems ['id'] = $item->id;
            $dataitems ['kode'] = $item->kode;
            $dataitems ['name'] = $item->nama;
        }


        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $stock,
            'barang'  => $dataitems
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(stockUpdatequest $request,StockBarang $stock)
    {
        //
        $stock->update($request->all());
        return redirect()->route('stock.index');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(StockBarang $stock)
    {
        $stock->delete();
        return redirect()->route('stock.index');
    }
}
